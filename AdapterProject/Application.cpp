#include "Application.hpp"
using namespace std;

//creates instance of thickPaint and uses functions to start a paint application
int main()
{
	ThickPaint tp;
	tp.saveConTitle();
	tp.setConTitle("CONSOLE PAINT APP");
	tp.saveConInfo();
	tp.resizeCon(70, 50);
	tp.startConBuffer();
}