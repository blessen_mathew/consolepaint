#include "ThickPaint.hpp"
using namespace std;

//THIS WRAPPER IS WHAT THIN SHOULD BE
//1.	Your wrappers / adapters are generic � don�t call them Paint
//THIN WRAPPER Doesn't directly access API functions
//Handles C++ types directly
//Hides and manages handle

Paint* pnt;	//pointer to the Paint object

//CTOR for the ThickPaint class
ThickPaint::ThickPaint()
{
	pnt = new Paint();
}

//Saves console title and stores it
void ThickPaint::saveConTitle()
{
	pnt->saveConsoleTitle(&pnt->getTitleVector());
}

//Sets the console title
void ThickPaint::setConTitle(string s)
{
	pnt->setConsoleTitle(s);
}

//Saves the console state so it can be resotred
void ThickPaint::saveConInfo()
{
	pnt->setHandles(pnt);
	pnt->saveScreenInfo(pnt);
}

//Restores the paint to its original state prior to resize
void ThickPaint::restoreConInfo()
{
	pnt->restoreConsoleInfo(pnt);
}

//Resizes the console to what user specifies
void ThickPaint::resizeCon(SHORT x, SHORT y)
{
	pnt->resizeWindow(pnt, x, y);
}

//Starts the read buffer so user can interact with console
void ThickPaint::startConBuffer()
{
	pnt->readBuffer(pnt);
}