#pragma once
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <Windows.h>
using namespace std;
class Paint {
private:
	HANDLE hConsoleInput, hConsoleOutput;
	CONSOLE_SCREEN_BUFFER_INFO	originalCSBI;
	CONSOLE_CURSOR_INFO			originalCCI;
	vector<CHAR_INFO>			originalBuffer;
	COORD						originalBufferCoord;
	DWORD						originalConsoleMode;
	WORD						currentConsoleWidth = 0;
	WORD WINDOW_WIDTH;
	WORD WINDOW_HEIGHT;
	vector<char> originalTitle;
	SMALL_RECT bufferRect{ 0 };
	DWORD consoleMode = ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT | ENABLE_MOUSE_INPUT | ENABLE_EXTENDED_FLAGS;
	DWORD oldConsoleMode;
	WORD colors[3] = { BACKGROUND_RED, BACKGROUND_BLUE, BACKGROUND_GREEN };
	DWORD numEvents;
	static Paint* _instance;
	BOOL done = false;
	HANDLE hStdin, hStdout;
	POINT pt;
	int color = 0;
	vector<INPUT_RECORD> inBuffer;

public:	
	Paint();
	void saveConsoleTitle(vector<char> *v);
	void setConsoleTitle(string title);
	void setHandles(Paint* p);
	void saveScreenInfo(Paint* p);
	vector<char>& getTitleVector();
	string getOriginalTitle(Paint* p);
	void resizeWindow(Paint* p, SHORT w, SHORT h);
	void restoreConsoleInfo(Paint* p);
	void paintCell(Paint* p, SHORT x, SHORT y);
	void MouseEventProc(MOUSE_EVENT_RECORD const& mer);
	void cls(Paint* p);
	void ProccessKeyEvent(KEY_EVENT_RECORD const& ker);
	int readConsoleInput();
	uint32_t getNumEvents();
	static BOOL CtrlHandler(DWORD ctrlType);
	void attachHandler();
	void readBuffer(Paint* p);
};