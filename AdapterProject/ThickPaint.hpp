#pragma once
#include "Paint.hpp"
using namespace std;

class ThickPaint {
private:
	Paint* pnt;

public:
	ThickPaint();
	void saveConTitle();
	void setConTitle(string s);
	void resizeCon(SHORT x, SHORT y);
	void restoreConInfo();
	void startConBuffer();
	void saveConInfo();
};