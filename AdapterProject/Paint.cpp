#include "XError.hpp"
#include "Paint.hpp"
using namespace std;

//This has HALF PAINT AND HALF THICK
//Segregate into seperate files

Paint* Paint::_instance;	//pointer to the paint instance

//CTOR that sets the static instance of paint to the current obejct and initializes the buffer vector
Paint::Paint()
{
	_instance = this;
	inBuffer = vector<INPUT_RECORD>(128);
	_instance->originalTitle = vector<char>(64 * 1024);
}

//Saves the console title to a char vector
void Paint::saveConsoleTitle(vector<char> *v)
{
	v->resize(GetConsoleTitleA(_instance->originalTitle.data(), (uint32_t)_instance->originalTitle.size()) + 1);
	v->shrink_to_fit();
}

//sets the console title 
void Paint::setConsoleTitle(string title)
{
	SetConsoleTitleA(title.c_str());
}

//sets handles for the pointer
void Paint::setHandles(Paint* p)
{
	p->hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	p->hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
}

//saves the console buffer info, the cursor info, console mode and attaches STD HANDLES
void Paint::saveScreenInfo(Paint* p)
{
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(p->hConsoleOutput, &p->originalCSBI));
	p->originalBuffer.resize(p->originalCSBI.dwSize.X*originalCSBI.dwSize.Y);
	p->originalBufferCoord = COORD{ 0 };
	p->bufferRect.Right = p->originalCSBI.dwSize.X - 1;
	p->bufferRect.Bottom = p->originalCSBI.dwSize.Y - 1;
	THROW_IF_CONSOLE_ERROR(ReadConsoleOutputA(p->hConsoleOutput, p->originalBuffer.data(), p->originalCSBI.dwSize, p->originalBufferCoord, &p->bufferRect));
	THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(p->hConsoleOutput, &p->originalCCI));
	GetConsoleMode(p->hStdin, &p->oldConsoleMode);
	attachHandler();
}

//returns the vector that stores the original title of console
vector<char>& Paint::getTitleVector()
{
	return _instance->originalTitle;
}

//returns the original title as a string
string Paint::getOriginalTitle(Paint* p)
{
	return std::string(p->getTitleVector().begin(), p->getTitleVector().end());
}

//resizes the console depending on the variables passed in
void Paint::resizeWindow(Paint* p, SHORT w, SHORT h)
{
	//set console info
	SMALL_RECT sr{ 0 };
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(p->hConsoleOutput, TRUE, &sr));

	//sets the buffersize
	COORD bufferSize;
	bufferSize.X = w;	//change this to entered param
	bufferSize.Y = h;
	THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(p->hConsoleOutput, bufferSize));

	CONSOLE_SCREEN_BUFFER_INFO sbi;
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(p->hConsoleOutput, &sbi));

	//sets the height of the window
	sr.Top = sr.Left = 0;
	w = std::min(w, sbi.dwMaximumWindowSize.X);
	h = std::min(h, sbi.dwMaximumWindowSize.Y);
	sr.Right = w - 1;
	sr.Bottom = h - 1;

	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(p->hConsoleOutput, TRUE, &sr));
	currentConsoleWidth = sr.Right - sr.Left + 1;

	//Hide the cursor
	auto newCCI = originalCCI;
	newCCI.bVisible = FALSE;
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(p->hConsoleOutput, &newCCI));

	p->hStdin = GetStdHandle(STD_INPUT_HANDLE);
	p->hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleMode(p->hStdin, consoleMode);
}

//restores the console to its original state
void Paint::restoreConsoleInfo(Paint* p)
{
	// Restore the original settings/size
	SMALL_RECT sr{ 0 };
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(p->hConsoleOutput, TRUE, &sr));
	THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(p->hConsoleOutput, p->originalCSBI.dwSize));
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(p->hConsoleOutput, TRUE, &p->originalCSBI.srWindow));

	// Restore the desktop contents
	bufferRect = SMALL_RECT{ 0 };
	bufferRect.Right = p->originalCSBI.dwSize.X - 1;
	bufferRect.Bottom = p->originalCSBI.dwSize.Y - 1;
	SetConsoleTitleA(p->originalTitle.data());
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(p->hConsoleOutput, p->originalBuffer.data(), p->originalCSBI.dwSize, p->originalBufferCoord, &p->bufferRect));

	// Restore the cursor
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(p->hConsoleOutput, &p->originalCCI));
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(p->hConsoleOutput, p->originalCSBI.dwCursorPosition));

	//set the console mode to original mode
	SetConsoleMode(p->hStdin, p->oldConsoleMode);
}

//Paints the cell at the coordinates passed in
void Paint::paintCell(Paint* p, SHORT x, SHORT y)
{
	GetCursorPos(&p->pt);
	string msg = " ";
	vector<WORD> attr{
		colors[color]	//the color defined in the WORD array
	};

	//the coordinatio of the mouse position
	COORD loc;
	loc.X = x;
	loc.Y = y;

	//Writes to the console and gives it the attribute defined
	DWORD nCharsWritten;
	WriteConsoleOutputCharacterA(p->hConsoleOutput, msg.c_str(), (DWORD)msg.size(), loc, &nCharsWritten);
	WriteConsoleOutputAttribute(p->hConsoleOutput, attr.data(), (DWORD)attr.size(), loc, &nCharsWritten);
}


//Handles mouse events (Left click and right click)
void Paint::MouseEventProc(MOUSE_EVENT_RECORD const& mer) {
	switch (mer.dwEventFlags) {
	
	// when a button is pressed or released
	case 0:	
	{
		auto mask = mer.dwButtonState;
		//If the left button is clicked, a cell is fileld with a specific color
		if (mask&FROM_LEFT_1ST_BUTTON_PRESSED)
		{
			paintCell(_instance,mer.dwMousePosition.X, mer.dwMousePosition.Y);
		}
		//if the right button is clicked, the specifiec color is changed. 
		if (mask&RIGHTMOST_BUTTON_PRESSED)
		{
			color++; 
			color = color % 3;
		}
	}
	break;

	//when the mouse is moved
	case MOUSE_MOVED:
		//If the left button is pressed down and mouse is moving, the cells in the path of the cursor are filled.
		if (GetKeyState(MK_LBUTTON) < 0)
			paintCell(_instance, mer.dwMousePosition.X, mer.dwMousePosition.Y);
		break;

	//default case
	default:
		break;
	}
}

//clears the current console screen
void Paint::cls(Paint* p)
{
	COORD coordScreen = { 0, 0 };    // home for the cursor 
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;

	// Get the number of character cells in the current buffer. 
	if (!GetConsoleScreenBufferInfo(p->hConsoleOutput, &csbi))
	{
		return;
	}

	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	// Fill the entire screen with blanks.
	if (!FillConsoleOutputCharacter(p->hConsoleOutput, (TCHAR) ' ', dwConSize, coordScreen, &cCharsWritten))
	{
		return;
	}

	// Get the current text attribute.
	if (!GetConsoleScreenBufferInfo(p->hConsoleOutput, &csbi))
	{
		return;
	}

	// Set the buffer's attributes accordingly.
	if (!FillConsoleOutputAttribute(p->hConsoleOutput, csbi.wAttributes, dwConSize, coordScreen, &cCharsWritten)) 
	{
		return;
	}
}

//Handles keyboard button presses
void Paint::ProccessKeyEvent(KEY_EVENT_RECORD const& ker)
{
	//if the button is 'c', the console will be cleared
	if (ker.uChar.AsciiChar == 'c')
	{
		cls(_instance);
	}
}

//reads the console input and uses the class member inBuffer
int Paint::readConsoleInput()
{
	return ReadConsoleInput(hStdin, inBuffer.data(), (DWORD)inBuffer.size(), &numEvents);
}


//returns the number of events
uint32_t Paint::getNumEvents()
{
	return _instance->numEvents;
}

//The ctrlHandler that will handle the CTRL_C event
BOOL Paint::CtrlHandler(DWORD ctrlType) {
	switch (ctrlType) {
	case CTRL_C_EVENT:
		//sets the member attribute 'done' to true
		_instance->done = TRUE;
		//restores the console to it's original state
		_instance->restoreConsoleInfo(_instance);
		return TRUE;
	}
	return FALSE;
}

//attaches the control handler of the console
void Paint::attachHandler()
{
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)Paint::CtrlHandler, TRUE);
}

//Begins reading the console buffer and sends events to the Mouse and Key events methods to see if any action needs to take place
void Paint::readBuffer(Paint* p)
{
	while (!p->done) {
		if (!p->readConsoleInput()) {
			cerr << "Failed to read console input\n";
			break;
		}

		//reads through the buffer for all the events
		for (size_t iEvent = 0; iEvent < p->getNumEvents(); ++iEvent) {
			switch (p->inBuffer[iEvent].EventType) {
			case MOUSE_EVENT:
				p->MouseEventProc(p->inBuffer[iEvent].Event.MouseEvent);
				break;
			case KEY_EVENT:
				p->ProccessKeyEvent(p->inBuffer[iEvent].Event.KeyEvent);
				break;
			}
		}
	}
}